use std::fs;
use clap::Parser;
use freedesktop_desktop_entry::{default_paths, DesktopEntry, Iter};

#[derive(Debug, Parser)]
#[clap(author, version, about)]
pub struct Opts {
    #[clap(short, long)]
    pub locale: Option<String>,
}

fn main() {
    let opts = Opts::parse();
    let locale = opts.locale.as_deref();

    for path in Iter::new(default_paths()) {
        if let Ok(bytes) = fs::read_to_string(&path) {
            if let Ok(entry) = DesktopEntry::decode(&path, &bytes) {
                let file_name = path.file_name().and_then(|s| s.to_str());
                let name = entry.name(locale);
                let exec = entry.exec();
                let generic_name = entry.generic_name(locale);
                let comment = entry.comment(locale);
                let keywords = entry.keywords();

                if let Some(file_name) = file_name {
                    let mut line = format!(": {}", file_name);

                    if let Some(keywords) = keywords {
                        line = format!("[{}] {}", keywords, line);
                    }

                    if let Some(comment) = comment {
                        line = format!("'{}' {}", comment, line);
                    }

                    if let Some(generic_name) = generic_name {
                        line = format!("({}) {}", generic_name, line);
                    }

                    if let Some(exec) = exec {
                        line = format!("`{}` {}", exec, line);
                    }

                    if let Some(name) = name {
                        line = format!("{} {}", name, line);
                    }

                    println!("{}", line);

                }
            }
        }
    }
}

